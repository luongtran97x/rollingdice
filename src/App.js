import logo from './logo.svg';
import './App.css';
import RollingDice from './RollingDice/RollingDice';

function App() {
  return (
  <div>
    <RollingDice></RollingDice>
  </div>
  );
}

export default App;
