import React, { Component } from "react";
import "./rolling.css";
import Dice from "./Dice";
import InfoGame from "./InfoGame";
import { connect } from "react-redux";
class RollingDice extends Component {
  render() {
    return (
      <div className="game">
        <div className="title-game text-center py-5 display-4">
          <p>Roling Dice</p>
        </div>
        <div class="row text-center mt-2">
          <div className="col-5">
            <button
              className="btnGame"
              onClick={() => {
                this.props.datCuoc(true);
              }}
            >
              TÀI
            </button>
          </div>
          <div className="col-2">
            <Dice></Dice>
          </div>
          <div className="col-2">
            <button
              className="btnGame"
              onClick={() => {
                this.props.datCuoc(false);
              }}
            >
              Xỉu
            </button>
          </div>
        </div>
        <div className="gameInfo text-center">
          <InfoGame></InfoGame>
          <button className="btn btn-success p-4 mt-5" onClick={this.props.playGame}>Play Game</button>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (option) => {
      dispatch({
        type: "DAT_CUOC",
        payload: option,
      });
    },
    playGame : () => {
      dispatch({
        type: "PLAY_GAME"
        
      })
    }
    
  };
};
export default connect(null, mapDispatchToProps)(RollingDice);
