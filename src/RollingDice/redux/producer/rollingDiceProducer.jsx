let initialState = {
  taiXiu: true, //true la tai (tu 3 den 11) , xiu  false (>12);
  mangXucXac: [
    { ma: 1, hinhAnh: "./img/1.png" },
    { ma: 2, hinhAnh: "./img/2.png" },
    { ma: 3, hinhAnh: "./img/3.png" },
  ],
  soBanThang: 0,
  soBanChoi: 0,
};

export const rollingDiceProducer = (state = initialState, action) => {
  switch (action.type) {
    case "DAT_CUOC": {
      state.taiXiu = action.payload;
      return { ...state };
    }
    case "PLAY_GAME": {
      // bước 1 xử lý random xúc sắc
      let mangNgauNhien = [];
      for (let i = 0; i < 3; i++) {
        let soNgauNhien = Math.floor(Math.random() * 6) + 1;
        let xucXacNgauNhien = {
          ma: soNgauNhien,
          hinhAnh: `./img/${soNgauNhien}.png`,
        };
        mangNgauNhien.push(xucXacNgauNhien);
      }
      state.mangXucXac = mangNgauNhien;
      state.soBanChoi += 1;
      let tongSoDiem = mangNgauNhien.reduce((tongDiem, xucXac) => {
        return (tongDiem += xucXac.ma);
      }, 0);
      if (
        (tongSoDiem > 11 && state.taiXiu === true) ||
        (tongSoDiem <= 11 && state.taiXiu === false)
      ) {
        state.soBanThang += 1;
      }
      return { ...state };
    }
    default:
      return state;
  }
};
