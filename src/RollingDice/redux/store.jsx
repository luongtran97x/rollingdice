import { composeWithDevTools } from "redux-devtools-extension";
import { rootReducer } from "./producer/rootReducer";
import { createStore } from "redux";

 
export const store = createStore(rootReducer,composeWithDevTools())