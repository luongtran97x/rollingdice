import React, { Component } from "react";
import { connect } from "react-redux";

class InfoGame extends Component {
  render() {
    const { taiXiu, soBanChoi, soBanThang } = this.props;
    return (
      <div>
        <div className="display-4">
          Bạn Chọn{" "}
          <span className="text-success">{taiXiu ? "Tài" : "Xỉu"}</span>{" "}
        </div>

        <div className="display-4">
          Số Lần Thắng <span className="text-warning">{soBanThang}</span>{" "}
        </div>

        <div className="display-4">
          Tổng Lượt Chơi <span className="text-danger">{soBanChoi}</span>{" "}
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    soBanThang: state.rollingDiceProducer.soBanThang,
    soBanChoi: state.rollingDiceProducer.soBanChoi,
    taiXiu: state.rollingDiceProducer.taiXiu,
  };
};

export default connect(mapStateToProps, null)(InfoGame);
