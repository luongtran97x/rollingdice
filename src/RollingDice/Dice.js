import React, { Component } from "react";
import { connect } from "react-redux";

class Dice extends Component {
  renderXucSac = (params) => {
    return this.props.mangXucSac.map((item, index) => {
      return <img src={item.hinhAnh} alt="1" className="mx-2" width={40}></img>;
    });
  };

  render() {
    return <div>{this.renderXucSac()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    mangXucSac: state.rollingDiceProducer.mangXucXac,
  };
};

export default connect(mapStateToProps, null)(Dice);
